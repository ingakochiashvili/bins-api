# Bins API

### Requirements

https://jsonbin.io/api-reference

The task is to create a series of automated tests that cover the CRUD-operations of jsonbin API

----

Cucumber/Gherkin test cases with implementations were created in the scope of the task.
The listed test cases are very basic scenarios for the API smoke testing and can be found in **ApiTest.feature** file.

- @TEST_1_Create_patient 
- @TEST_2_Read_patient_data
- @TEST_3_Update_patient_data
- @TEST_4_Delete_patient_data

Test parameters can be updated/added to each test case inside the apiTest.feature file 'Example' table.

**NOTICE**: Regarding "**@TEST_4_Delete_patient_data**" execution, a valid ID / list of IDs must be passed as a test parameter before each execution.

Examples of id: 

62580fa9bc312b30ebe75ba7;  
62580fd0c5284e31154d5d08;  
62581007bc312b30ebe75bbb;

Test execution status can be found on GitLab CI/CD
https://gitlab.com/ingakochiashvili/bins-api/-/pipelines

The feature file can be extended with different test scenarios according to the API documentation.

### Install and execution:
- Open Gitlab https://gitlab.com/ingakochiashvili/bins-api and click on Download tar.
- Open the downloaded folder from your IntelliJ -> Select apiTest.feature file and run.
