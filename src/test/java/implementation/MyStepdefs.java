package implementation;

import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static org.assertj.core.api.Assertions.*;

public class MyStepdefs {
    private static final String BASE_URL = "https://api.jsonbin.io/v3/b/";

    private final String xMasterKey = "$2b$10$PmlOIghWpyHSLmu0Hl1a4e3ht2VG/SZOdAp/p6g0ZguZMrh7O0ZXK";

    private Response responseDel;
    private Response response;
    private Response updaterDataResponse;

    private RequestSpecification getRequestObject(){
        return RestAssured.given()
                .header("X-Master-Key", xMasterKey)
                .header("Content-Type", "application/json");
    }

    @ParameterType(value = "true|True|TRUE|false|False|FALSE")
    public Boolean booleanValue(String value) {
        return Boolean.valueOf(value);
    }

    @When("I pass patients {string} {string} and {string} to BinsAPI")
    public void iCreatePatient(String name, String age, String allergy) {
        response = getRequestObject()
                .body(String.format("{ \"name\": \"%s\", \"age\": \"%s\",  \"allergy\": \"%s\" }",name,age,allergy))
                .post(BASE_URL);
    }

    @Then("patients are created")
    public void patientsAreCreated() {
        String id = response.body().jsonPath().getString("metadata.id");
        assertThat(id).withFailMessage("Patient creation failed").isNotEmpty();
    }

    @When("I pass patients id {string} to BinsAPI")
    public void iPassPatientsIdToBinsAPI(String patientId) {
        response = getRequestObject().get(BASE_URL + patientId);
    }

    @Then("I receive the status code 200 {booleanValue}")
    public void iReceiveThePatientData(boolean readPatient) {
        int statusCode = response.getStatusCode();
        assertThat(statusCode==200).isTrue().withFailMessage("Request Failed or invalid ID provided").isEqualTo(readPatient);
    }

    @When("I pass patients {string} {string} and {string} with existing {string} to BinsAPI")
    public void iPassPatientsAndWithExistingToBinsAPI(String name, String age, String allergy, String id) {
        response = getRequestObject()
                .body(String.format("{ \"name\": \"%s\", \"age\": \"%s\",  \"allergy\": \"%s\" }",name,age,allergy))
                .put(BASE_URL + id);
    }

    @And("I request patient data with {string}")
    public void iRequestPatientDataWith(String id) {
        updaterDataResponse = getRequestObject().get(BASE_URL + id);
    }

    @Then("I receive patient data with {string} {string} and {string}")
    public void iReceivePatientDataWithAnd(String name, String age, String allergy) {
        String returnedName = updaterDataResponse.body().jsonPath().getString("record.name");
        String returnedAge =  updaterDataResponse.body().jsonPath().getString("record.age");
        String returnedAllergy =  updaterDataResponse.body().jsonPath().getString("record.allergy");
        assertThat(returnedName).withFailMessage("Name was not updated").isEqualTo(name);
        assertThat(returnedAge).withFailMessage("Age was not updated").isEqualTo(age);
        assertThat(returnedAllergy).withFailMessage("Allergy was not updated").isEqualTo(allergy);
    }

    @When("I pass patients id {string} to BinsAPI with delete method")
    public void iPassPatientsIdToBinsAPIWithDeleteMethod(String id) {
            responseDel = getRequestObject().delete(BASE_URL + id);
        }

    @Then("I receive message {string}")
    public void message(String message) {
        String outputMessage = responseDel.body().jsonPath().getString("message");
        assertThat(outputMessage).withFailMessage("Invalid ID provided").isEqualTo(message);
        int statusCode = responseDel.getStatusCode();
        assertThat(statusCode).withFailMessage("Request Failed").isEqualTo(200);
    }
}
