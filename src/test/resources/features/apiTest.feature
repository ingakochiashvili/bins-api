Feature: Post/Read/Update/Delete items in Bin

   @TEST_1_Create_patient
  Scenario Outline:
    When I pass patients "<name>" "<age>" and "<allergy>" to BinsAPI
    Then patients are created
    Examples:
      | name | age | allergy |
      | Anna | 34  | none    |
      | Nino | 55  | Nuts    |

  @TEST_2_Read_patient_data
  Scenario Outline:
    When I pass patients id "<id>" to BinsAPI
    Then I receive the status code 200 true
    Examples:
      | id                       |
      | 6257ff09c5284e31154d5810 |
      | 6258074180883c3054e1851a |

  @TEST_3_Update_patient_data
  Scenario Outline:
    When I pass patients "<name>" "<age>" and "<allergy>" with existing "<id>" to BinsAPI
    And I request patient data with "<id>"
    Then I receive patient data with "<name>" "<age>" and "<allergy>"
    Examples:
      | name | age | allergy | id                       |
      | Tim  | 47  | Honey   | 625806efc5284e31154d5a6e |
      | Anna | 36  | Dust    | 625806e3bc312b30ebe75905 |

  @TEST_4_Delete_patient_data
  Scenario Outline:
    When I pass patients id "<id>" to BinsAPI with delete method
    Then I receive message "Bin deleted successfully"
    Examples:
      | id                        |
      | 62581dd180883c3054e18b53  |
